/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Gebruiker
 */
public class ProbeMatchResultsReaderXMLimpl implements ProbeMatchResultsReader {
    private ProbeMatchResults probeMatchResults;
    private List<Integer> probeTargetIDs;
    private AnalysisMode analysisMode;

    @Override
    public ProbeMatchResults read(String fileName) throws IOException {
        try {
            if (fileName == null) {
                throw new IOException("no file name was given to read from");
            }
            this.probeMatchResults = new ProbeMatchResults();

            //open xml file
            File xmlFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();
            
            //read analysis settings
            NodeList nList;
            Node sNode;
            nList = doc.getElementsByTagName(ProbeMatcherStringConstants.ANALYSIS_SETTINGS);
            if(nList.getLength() != 1) throw new IOException("xml file seems illegally formatted: no or multiple " + ProbeMatcherStringConstants.ANALYSIS_SETTINGS + " tags");
            sNode = nList.item(0);
            readAnalysisSettings(sNode);
            
            //create probe objects
            creatProbeObjects();
            
            //read meta data
            nList = doc.getElementsByTagName(ProbeMatcherStringConstants.META_DATA);
            if(nList.getLength() != 1) throw new IOException("xml file seems illegally formatted: no or multiple " + ProbeMatcherStringConstants.META_DATA + " tags");
            sNode = nList.item(0);
            readMetaData(sNode);
            
            //read match statistics
            nList = doc.getElementsByTagName(ProbeMatcherStringConstants.MATCH_STATISTIC);
            if(nList.getLength() == 0) throw new IOException("xml file seems illegally formatted: no " + ProbeMatcherStringConstants.MATCH_STATISTICS + " tags");
            //sNode = nList.item(0);
            readMatchStatistics(nList);
            
            //read sequence represented nodes (search space)
            nList = doc.getElementsByTagName(ProbeMatcherStringConstants.SEQUENCE_REPRESENTED_NODE);
            if(nList.getLength() == 0) throw new IOException("xml file seems illegally formatted: no " + ProbeMatcherStringConstants.MATCH_STATISTICS + " tags");
            //sNode = nList.item(0);
            readSequenceSearchSpace(nList);
            
            return probeMatchResults;
        } catch (ParserConfigurationException ex) {
            throw new IOException("an error occurred; cause=" + ex.getCause() + " message=" + ex.getMessage());
        } catch (SAXException ex) {
            throw new IOException("an error occurred; cause=" + ex.getCause() + " message=" + ex.getMessage());
        }
    }

    private void readSequenceSearchSpace(NodeList nList) throws IOException {
        for( int i=0 ; i < nList.getLength(); i++){
            Node nsc = nList.item(i);
            //System.out.println(nsc.getNodeName() + ": " + nsc.getTextContent());
            if (nsc.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nsc;
                int taxID = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.TAX_ID));
                int seqCount = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.COUNT));
                this.probeMatchResults.setRepresentedTaxID(taxID, seqCount);
                //System.out.println("taxID=" + taxID + "; seqCount=" + seqCount);
            }
        }
    }

    private void readMatchStatistics(NodeList nList) throws IOException {
        //iterate match stats nodes
        for( int msc=0 ; msc < nList.getLength(); msc++){
            Node msn = nList.item(msc);
            int targetID = 0;
            assert msn.getNodeName().equals(ProbeMatcherStringConstants.MATCH_STATISTIC);
            if (msn.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) msn;
                targetID = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.TARGET_GROUP_ID));
                //System.out.println("targetID=" + targetID);
            }
            if(targetID == 0) throw new IOException("no target ID could be read for match statistics node");
            ProbeMatchStatistics pms = new ProbeMatchStatistics();//this.probeMatchResults.getProbeMatchStatistics(targetID);
            this.probeMatchResults.addTargetGroupProbeMatchStatistics(targetID, pms);
            
            NodeList childNodes = msn.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node n = childNodes.item(i);

                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    switch(n.getNodeName()){
                        case ProbeMatcherStringConstants.TARGET_GROUP_SPECIES_COUNT:{
                            pms.setTargetGroupSpeciesNumber(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_LEAF_COUNT:{
                            pms.setTargetGroupLeafNumber(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_REPRESENTED_SEQUENCES:{
                            pms.setTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_REPRESENTED_NODES:{
                            pms.setTargetGroupRepresentedNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_REPRESENTED_SEQUENCES:{
                            pms.setNonTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_REPRESENTED_NODES:{
                            pms.setNonTargetGroupRepresentedNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_FW_PROBE_MATCHED_SEQUENCES:{
                            pms.setProbeFWmatchedTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_FW_PROBE_MATCHED_NODES:{
                            pms.setProbeFWmatchedTargetGroupNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_FW_PROBE_MATCHED_SEQUENCES:{
                            pms.setProbeFWmatchedTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_FW_PROBE_MATCHED_NODES:{
                            pms.setProbeFWmatchedNonTargetGroupNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_RV_PROBE_MATCHED_SEQUENCES:{
                            pms.setProbeRVmatchedTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_RV_PROBE_MATCHED_NODES:{
                            pms.setProbeRVmatchedTargetGroupNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_RV_PROBE_MATCHED_SEQUENCES:{
                            pms.setProbeRVmatchedNonTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_RV_PROBE_MATCHED_NODES:{
                            pms.setProbeRVmatchedNonTargetGroupNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_BOTH_PROBES_MATCHED_SEQUENCES:{
                            pms.setProbeCombiMatchedTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_BOTH_PROBES_MATCHED_NODES:{
                            pms.setProbeCombiMatchedTargetGroupNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_BOTH_PROBES_MATCHED_NODES:{
                            pms.setProbeCombiMatchedNonTargetGroupSequences(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_BOTH_PROBES_MATCHED_SEQUENCES:{
                            pms.setProbeCombiMatchedNonTargetGroupNodes(Integer.parseInt(n.getTextContent()));
                            break;
                        }
                        case ProbeMatcherStringConstants.TARGET_GROUP_FRAGMENT_SIZES:{
                            processFragmentSizes(n, pms, true);
                            break;
                        }
                        case ProbeMatcherStringConstants.NON_TARGET_GROUP_FRAGMENT_SIZES:{
                            processFragmentSizes(n, pms, false);
                            break;
                        }
                        case ProbeMatcherStringConstants.TAXNODE_MATCHES:{
                            processTaxNodeMatches(n, pms);
                            break;
                        }
                    }
                }
            }
        }
    }

    private void processTaxNodeMatches(Node tnmNode, ProbeMatchStatistics pms) {
        NodeList childNodes = tnmNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node n = childNodes.item(i);

            if (n.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) n;
                int taxID = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.TAX_ID));
                int fwMatches = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.PROBE_FW_MATCHES));
                boolean isTG = Boolean.parseBoolean(eElement.getAttribute(ProbeMatcherStringConstants.TARGET_GROUP));
                if(analysisMode == AnalysisMode.PRIMER_PAIR){
                    int rvMatches = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.PROBE_RV_MATCHES));
                    int pairMatches = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.PROBE_PAIR_MATCHES));
                    pms.addTaxNodeMatchResults(new TaxNodeMatchResults(taxID, isTG, fwMatches, rvMatches, pairMatches));
                    //System.out.println("" + new TaxNodeMatchResults(taxID, isTG, fwMatches, rvMatches, pairMatches));
                }
                else{
                    pms.addTaxNodeMatchResults(new TaxNodeMatchResults(taxID, isTG, fwMatches));
                    //System.out.println("" + new TaxNodeMatchResults(taxID, isTG, fwMatches));
                }
            }
        }
    }

    /**
     * processes the fragment sizes (only for primer pair results
     * @param tnmNode
     * @param pms 
     */
    private void processFragmentSizes(Node tnmNode, ProbeMatchStatistics pms, boolean targetGroup){
        NodeList childNodes = tnmNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node n = childNodes.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) n;
                int fragmentSize = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.SIZE));
                int count = Integer.parseInt(eElement.getAttribute(ProbeMatcherStringConstants.COUNT));
                if(targetGroup) pms.setTargetgroupFragmentSizeCount(fragmentSize, count);
                else pms.setNonTargetgroupFragmentSizeCount(fragmentSize, count);
            }
        }
    }
    
    /**
     * reads and processes the metadata node
     * @param sNode 
     */
    private void readMetaData(Node sNode) {
        NodeList childNodes = sNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node n = childNodes.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                //System.out.println(n.getNodeName() + ": " + n.getTextContent());
                switch(n.getNodeName()){
                    case ProbeMatcherStringConstants.DISAMBIGUED_FORWARD_PROBES:{
                        //this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT, n.getTextContent());
                        processDisambiguedProbes(n, true);
                        break;
                    }
                    case ProbeMatcherStringConstants.DISAMBIGUED_REVERSE_PROBES:{
                        //this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT, n.getTextContent());
                        processDisambiguedProbes(n, false);
                        break;
                    }
                    case ProbeMatcherStringConstants.TREE_SIZE:{
                        this.probeMatchResults.setTreeSize(Integer.parseInt(n.getTextContent()));//getAnalysisSettings().put(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.UNFETCHABLE_NODES:{
                        this.probeMatchResults.setUnfetchableNodesCount(Integer.parseInt(n.getTextContent()));
                        break;
                    }
                    case ProbeMatcherStringConstants.SEQUENCE_COUNT:{
                        this.probeMatchResults.setSequenceCount(Integer.parseInt(n.getTextContent()));
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * processes disambigued probes into Probe objects
     * @param pNode
     * @param forward forward or reverse probe
     */
    private void processDisambiguedProbes(Node pNode, boolean forward) {
        NodeList childNodes = pNode.getChildNodes();
        String append = "";
        Probe p;
        if(forward) p = this.probeMatchResults.getForwardProbe();
        else{
            p = this.probeMatchResults.getReverseProbe();
            append = " (reverse_complemented)";
        }
        List<Probe> dap = new ArrayList<Probe>();
        int serial = 1;
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node n = childNodes.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                //System.out.println(n.getNodeName() + ": " + n.getTextContent());
                assert n.getNodeName().equals(ProbeMatcherStringConstants.DISAMBIGUED_PROBE);
                Probe np = new Probe((p.getName() + serial + append), n.getTextContent(), forward, this.probeTargetIDs);
                dap.add(np);
                serial++;
            }
        }
        if(forward) this.probeMatchResults.setExpandedProbesFW(dap);
        else this.probeMatchResults.setExpandedProbesRV(dap);
    }
    
    /**
     * create Probe objects from string data
     */
    private void creatProbeObjects(){
        Probe fw = new Probe(
                this.probeMatchResults.getAnalysisSettings().get(ProbeMatcherStringConstants.FORWARD_PROBE_NAME), 
                this.probeMatchResults.getAnalysisSettings().get(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE), 
                true, probeTargetIDs);
        probeMatchResults.setForwardProbe(fw);
        
        if(analysisMode == AnalysisMode.PRIMER_PAIR){
            Probe rv = new Probe(
                    this.probeMatchResults.getAnalysisSettings().get(ProbeMatcherStringConstants.REVERSE_PROBE_NAME), 
                    this.probeMatchResults.getAnalysisSettings().get(ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE), 
                    false, probeTargetIDs);
            probeMatchResults.setReverseProbe(rv);
        }
        //System.out.println("FW=" + this.probeMatchResults.getForwardProbe() + "; RV=" + this.probeMatchResults.getReverseProbe());
    }
    /**
     * processes the analysis settings node
     * @param sNode 
     */
    private void readAnalysisSettings(Node sNode) {
        NodeList childNodes = sNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node n = childNodes.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                //System.out.println(n.getNodeName() + ": " + n.getTextContent());
                switch(n.getNodeName()){
                    case ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.ANALYSIS_MODE:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.ANALYSIS_MODE, n.getTextContent());
                        this.analysisMode = AnalysisMode.valueOf(n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.FORWARD_PROBE_NAME:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.FORWARD_PROBE_NAME, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.MAX_DISTANCE:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.MAX_DISTANCE, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.REVERSE_PROBE_NAME:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.REVERSE_PROBE_NAME, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.SEQUENCE_FILE_PATH:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.SEQUENCE_FILE_PATH, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.TARGET_TAX_IDS:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.TARGET_TAX_IDS, n.getTextContent());
                        List<Integer> probeTargetIDs = new ArrayList<Integer>();
                        for (String tid : n.getTextContent().split(",")) {
                            probeTargetIDs.add(Integer.parseInt(tid));
                        }
                        this.probeTargetIDs = probeTargetIDs;
                        break;
                    }
                    case ProbeMatcherStringConstants.THREE_PRIME_END_SIZE:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.THREE_PRIME_END_SIZE, n.getTextContent());
                        break;
                    }
                    case ProbeMatcherStringConstants.TIME_STAMP:{
                        this.probeMatchResults.getAnalysisSettings().put(ProbeMatcherStringConstants.TIME_STAMP, n.getTextContent());
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * testing main
     * @param args 
     */
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("ProbeMatcherAPI");
        try {
            logger.addHandler(new FileHandler("C:\\Users\\Gebruiker\\Documents\\PriDEvWorkspace\\log.txt"));
            logger.removeHandler(null);
            //throw new IllegalArgumentException("TEST");
            ProbeMatchResultsReaderXMLimpl r = new ProbeMatchResultsReaderXMLimpl();
            r.read("C:\\Users\\Gebruiker\\Documents\\PriDEvWorkspace\\methanomicrobiales_matching.xml");
            System.out.println("finished");
        } catch (IOException ex) {
            Logger.getLogger(ProbeMatchResultsReaderXMLimpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(ProbeMatchResultsReaderXMLimpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
