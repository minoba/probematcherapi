/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

/**
 *
 * @author Gebruiker
 */
public enum MatchStatisticSelection {
    TRUE_POSITIVES ("True Positives"),
    FALSE_POSITIVES ("False Positives"),
    TRUE_NEGATIVES ("True Negatives"),
    FALSE_NEGATIVES ("False Negatives");
    private String type;
    private MatchStatisticSelection(String type){
        this.type = type;
    }
    public String toString(){
        return type;
    }
}
