/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Abstracts away all statistics of probe matching, per taxonomic target group
 * @author m.a.noback (michiel@cellingo.net)
 */
public class ProbeMatchStatistics {
    /**
     * this field encapsulates all match data with taxID as key
     */
    private HashMap<Integer, TaxNodeMatchResults> taxNodeMatchResults = new HashMap<Integer, TaxNodeMatchResults>();
    /*target groups match sizes for primer pairs: key=size of fragment, value=occurrences*/
    private final HashMap<Integer, Integer> targetGroupsFragmentSizes = new HashMap<Integer, Integer>();
    /*non-target groups match sizes for primer pairs: key=size of fragment, value=occurrences*/
    private final HashMap<Integer, Integer> nonTargetGroupsFragmentSizes = new HashMap<Integer, Integer>();

    
    private int targetGroupSpeciesNumber = 0;
    private int targetGroupLeafNumber = 0;
    //private int treeSize = 0;
    private int targetGroupSequences = 0;
    private int targetGroupRepresentedNodes = 0;
    private int nonTargetGroupSequences = 0;
    private int nonTargetGroupRepresentedNodes = 0;
    private int probeFWmatchedTargetGroupSequences;
    private int probeFWmatchedNonTargetGroupSequences;
    private int probeRVmatchedTargetGroupSequences;
    private int probeRVmatchedNonTargetGroupSequences;
    private int probeCombiMatchedTargetGroupSequences;
    private int probeCombiMatchedNonTargetGroupSequences;

    private int probeFWmatchedTargetGroupNodes;
    private int probeFWmatchedNonTargetGroupNodes;
    private int probeRVmatchedTargetGroupNodes;
    private int probeRVmatchedNonTargetGroupNodes;
    private int probeCombiMatchedTargetGroupNodes;
    private int probeCombiMatchedNonTargetGroupNodes;

    /**
     * adds a size of a matched primer pair fragment within target group - not necessary in the correct size range!
     * @param size the fragment size
     */
    public void addTargetGroupFragmentSize(int size){
        if(targetGroupsFragmentSizes.containsKey(size)){
            targetGroupsFragmentSizes.put(size, targetGroupsFragmentSizes.get(size)+1);
        }
        else{
            targetGroupsFragmentSizes.put(size, 1);
        }
    }
    
    /**
     * sets the count for a specific size, target group
     * @param size
     * @param count 
     */
    public void setTargetgroupFragmentSizeCount(int size, int count){
        targetGroupsFragmentSizes.put(size, count);
    }
    
    /**
     * returns the found fragment sizes of primer pair matches within target group
     * @return targetGroupsFragmentSizes
     */
    public HashMap<Integer, Integer> getTargetGroupsFragmentSizes(){
        return targetGroupsFragmentSizes;
    }
    
    /**
     * adds a size of a matched primer pair fragment outside target group - not necessary in the correct size range!
     * @param size the fragment size
     */    
    public void addNonTargetGroupFragmentSize(int size){
        if(nonTargetGroupsFragmentSizes.containsKey(size)){
            nonTargetGroupsFragmentSizes.put(size, nonTargetGroupsFragmentSizes.get(size)+1);
        }
        else{
            nonTargetGroupsFragmentSizes.put(size, 1);
        }
    }
    
    
    /**
     * sets the count for a specific size, non-target group
     * @param size
     * @param count 
     */
    public void setNonTargetgroupFragmentSizeCount(int size, int count){
        nonTargetGroupsFragmentSizes.put(size, count);
    }
    
    
    /**
     * returns the found fragment sizes of primer pair matches outside target group
     * @return nonTargetGroupsFragmentSizes
     */
    public HashMap<Integer, Integer> getNonTargetGroupsFragmentSizes(){
        return nonTargetGroupsFragmentSizes;
    }
    
    /**
     * checks the existence of a TaxNodeMatchResults member with the given taxID
     * @param taxID
     * @return member exists
     */
    public boolean hasTaxNodeMatcheResults(int taxID){
        return this.taxNodeMatchResults.containsKey(taxID);
    }
    
    /**
     * adds a TaxNodeMatchResults
     * @param taxNodeMatchResults
     * @throws IllegalArgumentException when the member already exists
     */
    public void addTaxNodeMatchResults(TaxNodeMatchResults taxNodeMatchResults){
        if(this.taxNodeMatchResults.containsKey(taxNodeMatchResults.getTaxID())){
            throw new IllegalArgumentException("this collection already contains a member with taxID " + taxNodeMatchResults.getTaxID());
        }
        this.taxNodeMatchResults.put(taxNodeMatchResults.getTaxID(), taxNodeMatchResults);
    }
    
    /**
     * returns the TaxNodeMatchResults member woth the given taxID
     * @param taxID
     * @return 
     */
    public TaxNodeMatchResults getTaxNodeMatchResults(int taxID){
        return this.taxNodeMatchResults.get(taxID);
    }
    
    /**
     * returns a list basesd on a statistics subset selection
     * @param selection
     * @return list
     */
    public List<TaxNodeMatchResults> getTaxNodeMatchResults(MatchStatisticSelection selection){
        List<TaxNodeMatchResults> l = new ArrayList<TaxNodeMatchResults>();
        
        for(int taxID : this.taxNodeMatchResults.keySet()){
            TaxNodeMatchResults tnmr = this.taxNodeMatchResults.get(taxID);
            if( selection == MatchStatisticSelection.TRUE_POSITIVES && tnmr.isTargetGroup() ){
                l.add(tnmr);
            }
            else if( selection == MatchStatisticSelection.FALSE_POSITIVES && ! tnmr.isTargetGroup() ){
                l.add(tnmr);
            }
        }
        return l;
    }
    
    /**
     * returns the desired type of iterator for this collections' taxNodeMatchResults
     * @param iteratorType
     * @return iterator
     */
    public Iterator<TaxNodeMatchResults> iterator( IteratorType iteratorType ){
        return iterator(iteratorType, null);
    }
    
    /**
     * returns the desired type of iterator for this collections' taxNodeMatchResults, but passes all 
     * TaxNodeMatchResults objects throught the given filter
     * @param iteratorType
     * @param filter
     * @return iterator
     */
    public Iterator<TaxNodeMatchResults> iterator( IteratorType iteratorType, TaxNodeMatchResultsFilter filter ){
        ArrayList<TaxNodeMatchResults> temp = new ArrayList<TaxNodeMatchResults>();
        for( TaxNodeMatchResults r : this.taxNodeMatchResults.values() ){
            if( filter == null || filter.filter(r)) temp.add(r);
        }
        switch(iteratorType){
            case BY_MATCH_COUNT_FORWARD: {
                Collections.sort(temp, new Comparator<TaxNodeMatchResults>(){

                    @Override
                    public int compare(TaxNodeMatchResults o1, TaxNodeMatchResults o2) {
                        return o2.getForwardStrandMatchCount() - o1.getForwardStrandMatchCount();
                    }
                });
                return temp.iterator();
            }
            case BY_MATCH_COUNT_REVERSE: {
                Collections.sort(temp, new Comparator<TaxNodeMatchResults>(){

                    @Override
                    public int compare(TaxNodeMatchResults o1, TaxNodeMatchResults o2) {
                        return o2.getReverseStrandMatchCount() - o1.getReverseStrandMatchCount();
                    }
                });
                return temp.iterator();
            }

            case BY_MATCH_COUNT_BOTH: {
                Collections.sort(temp, new Comparator<TaxNodeMatchResults>(){

                    @Override
                    public int compare(TaxNodeMatchResults o1, TaxNodeMatchResults o2) {
                        return o2.getBothStrandMatchCount() - o1.getBothStrandMatchCount();
                    }
                });
                return temp.iterator();
            }
            case BY_TAX_ID: {
                Collections.sort(temp, new Comparator<TaxNodeMatchResults>(){

                    @Override
                    public int compare(TaxNodeMatchResults o1, TaxNodeMatchResults o2) {
                        return o1.getTaxID() - o2.getTaxID();
                    }
                });
                return temp.iterator();
            }
            case RANDOM: return this.taxNodeMatchResults.values().iterator();
            default: return this.taxNodeMatchResults.values().iterator();
        }
    }
    
    /**
     * returns the count for the number of TaxNodeMatchResults instances passing the filter
     * @param filter
     * @return passedCount
     */
    public int getPassedCount(TaxNodeMatchResultsFilter filter){
        int passedCount = 0;
        Iterator<TaxNodeMatchResults> iterator = iterator(IteratorType.RANDOM, filter);
        while(iterator.hasNext()){
            TaxNodeMatchResults tnmr = iterator.next();
            if(filter.filter(tnmr)) passedCount++;
        }
        return passedCount;
    }
    
    public int getTargetGroupSpeciesNumber() {
        return targetGroupSpeciesNumber;
    }

    public void setTargetGroupSpeciesNumber(int targetGroupSpeciesNumber) {
        this.targetGroupSpeciesNumber = targetGroupSpeciesNumber;
    }

    public int getTargetGroupLeafNumber() {
        return targetGroupLeafNumber;
    }

    public void setTargetGroupLeafNumber(int targetGroupLeafNumber) {
        this.targetGroupLeafNumber = targetGroupLeafNumber;
    }

    public int getTargetGroupSequences() {
        return targetGroupSequences;
    }

    public void setTargetGroupSequences(int targetGroupSequences) {
        this.targetGroupSequences = targetGroupSequences;
    }

    public int getTargetGroupRepresentedNodes() {
        return targetGroupRepresentedNodes;
    }

    public void setTargetGroupRepresentedNodes(int targetGroupRepresentedNodes) {
        this.targetGroupRepresentedNodes = targetGroupRepresentedNodes;
    }

    public int getNonTargetGroupSequences() {
        return nonTargetGroupSequences;
    }

    public void setNonTargetGroupSequences(int nonTargetGroupSequences) {
        this.nonTargetGroupSequences = nonTargetGroupSequences;
    }

    public int getNonTargetGroupRepresentedNodes() {
        return nonTargetGroupRepresentedNodes;
    }

    public void setNonTargetGroupRepresentedNodes(int nonTargetGroupRepresentedNodes) {
        this.nonTargetGroupRepresentedNodes = nonTargetGroupRepresentedNodes;
    }

    @Override
    public String toString() {
        return "ProbeMatchStatistics{" + "\n\ttargetGroupSpeciesNumber=" + targetGroupSpeciesNumber 
                + "\n\t, targetGroupLeafNumber=" + targetGroupLeafNumber 
                + "\n\t, targetGroupSequences=" + targetGroupSequences 
                + "\n\t, targetGroupRepresentedNodes=" + targetGroupRepresentedNodes 
                + "\n\t, nonTargetGroupSequences=" + nonTargetGroupSequences 
                + "\n\t, nonTargetGroupRepresentedNodes=" + nonTargetGroupRepresentedNodes + '}';
    }

    public void setProbeFWmatchedTargetGroupSequences(int probeFWmatchedTargetGroupSequences) {
        this.probeFWmatchedTargetGroupSequences = probeFWmatchedTargetGroupSequences;
    }

    public void setProbeFWmatchedNonTargetGroupSequences(int probeFWmatchedNonTargetGroupSequences) {
        this.probeFWmatchedNonTargetGroupSequences = probeFWmatchedNonTargetGroupSequences;
    }

    public void setProbeRVmatchedTargetGroupSequences(int probeRVmatchedTargetGroupSequences) {
        this.probeRVmatchedTargetGroupSequences = probeRVmatchedTargetGroupSequences;
    }

    public void setProbeRVmatchedNonTargetGroupSequences(int probeRVmatchedNonTargetGroupSequences) {
        this.probeRVmatchedNonTargetGroupSequences = probeRVmatchedNonTargetGroupSequences;
    }

    public void setProbeCombiMatchedTargetGroupSequences(int probeCombiMatchedTargetGroupSequences) {
        this.probeCombiMatchedTargetGroupSequences = probeCombiMatchedTargetGroupSequences;
    }

    public void setProbeCombiMatchedNonTargetGroupSequences(int probeCombiMatchedNonTargetGroupSequences) {
        this.probeCombiMatchedNonTargetGroupSequences = probeCombiMatchedNonTargetGroupSequences;
    }

    public int getProbeFWmatchedTargetGroupSequences() {
        return probeFWmatchedTargetGroupSequences;
    }

    public int getProbeFWmatchedNonTargetGroupSequences() {
        return probeFWmatchedNonTargetGroupSequences;
    }

    public int getProbeRVmatchedTargetGroupSequences() {
        return probeRVmatchedTargetGroupSequences;
    }

    public int getProbeRVmatchedNonTargetGroupSequences() {
        return probeRVmatchedNonTargetGroupSequences;
    }

    public int getProbeCombiMatchedTargetGroupSequences() {
        return probeCombiMatchedTargetGroupSequences;
    }

    public int getProbeCombiMatchedNonTargetGroupSequences() {
        return probeCombiMatchedNonTargetGroupSequences;
    }

    public int getProbeFWmatchedTargetGroupNodes() {
        return probeFWmatchedTargetGroupNodes;
    }

    public void setProbeFWmatchedTargetGroupNodes(int probeFWmatchedTargetGroupNodes) {
        this.probeFWmatchedTargetGroupNodes = probeFWmatchedTargetGroupNodes;
    }

    public int getProbeFWmatchedNonTargetGroupNodes() {
        return probeFWmatchedNonTargetGroupNodes;
    }

    public void setProbeFWmatchedNonTargetGroupNodes(int probeFWmatchedNonTargetGroupNodes) {
        this.probeFWmatchedNonTargetGroupNodes = probeFWmatchedNonTargetGroupNodes;
    }

    public int getProbeRVmatchedTargetGroupNodes() {
        return probeRVmatchedTargetGroupNodes;
    }

    public void setProbeRVmatchedTargetGroupNodes(int probeRVmatchedTargetGroupNodes) {
        this.probeRVmatchedTargetGroupNodes = probeRVmatchedTargetGroupNodes;
    }

    public int getProbeRVmatchedNonTargetGroupNodes() {
        return probeRVmatchedNonTargetGroupNodes;
    }

    public void setProbeRVmatchedNonTargetGroupNodes(int probeRVmatchedNonTargetGroupNodes) {
        this.probeRVmatchedNonTargetGroupNodes = probeRVmatchedNonTargetGroupNodes;
    }

    public int getProbeCombiMatchedTargetGroupNodes() {
        return probeCombiMatchedTargetGroupNodes;
    }

    public void setProbeCombiMatchedTargetGroupNodes(int probeCombiMatchedTargetGroupNodes) {
        this.probeCombiMatchedTargetGroupNodes = probeCombiMatchedTargetGroupNodes;
    }

    public int getProbeCombiMatchedNonTargetGroupNodes() {
        return probeCombiMatchedNonTargetGroupNodes;
    }

    public void setProbeCombiMatchedNonTargetGroupNodes(int probeCombiMatchedNonTargetGroupNodes) {
        this.probeCombiMatchedNonTargetGroupNodes = probeCombiMatchedNonTargetGroupNodes;
    }

    
    
    /**
     * Iteration types for traversing this collection
     */
    public static enum IteratorType{
        RANDOM,
        BY_TAX_ID,
        BY_MATCH_COUNT_BOTH,
        BY_MATCH_COUNT_FORWARD,
        BY_MATCH_COUNT_REVERSE;
    }
    
}
