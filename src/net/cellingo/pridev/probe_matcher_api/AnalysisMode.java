/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

/**
 * Analysis mode enum for specifying analysis mode of probe matching procedure
 * @author m.a.noback (michiel@cellingo.net)
 */
public enum AnalysisMode {

    SINGLE_PROBE,
    PRIMER_PAIR;
}
