/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import net.cellingo.sequence_tools.alphabets.DnaSequenceAlphabet;
import net.cellingo.sequence_tools.sequences.NucleicAcidSequence;

/**
 *
 * @author Gebruiker
 */
public class Probe {
    private String name;
    private String sequence;
    private boolean forwardStrand;
    private List<Integer> targetNodeIds;

    public Probe(){}
    
    public Probe(String name, String sequence, boolean forwardStrand, List<Integer> targetNodeIds) {
        this.name = name;
        this.sequence = sequence;
        this.forwardStrand = forwardStrand;
        this.targetNodeIds = targetNodeIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public void setForwardStrand(boolean forwardStrand) {
        this.forwardStrand = forwardStrand;
    }

    public void setTargetNodeIds(List<Integer> targetNodeIds) {
        this.targetNodeIds = targetNodeIds;
    }

    
    public String getSequence() {
        return sequence;
    }

    public boolean isForwardStrand() {
        return forwardStrand;
    }

    public List<Integer> getTargetNodeIds() {
        return targetNodeIds;
    }

    @Override
    public String toString() {
        return "Probe{" + "name=" + name + ", sequence=" + sequence + ", forwardStrand=" + forwardStrand + ", targetNodeIds=" + targetNodeIds + '}';
    }
    
    /**
     * expands this probes sequence on ambiguous bases. E.g. The base Y wil result in two probe objects, 
     * one with a C and one with a T at the respective position. 
     * @return expandedProbeList 
     */
    public List<Probe> expandIUPACambiguities(){
        //iterate the DNA characdters
        List<StringBuilder> allProbeSeqs = new ArrayList<StringBuilder>();
        List<StringBuilder> tempSeqs = new ArrayList<StringBuilder>();
        int position = 0;
        for( Character c : this.sequence.toCharArray() ){
            position++;
            Character[] ambBases = DnaSequenceAlphabet.getAmbiguousBases(c);
            //System.out.println("" + Arrays.toString(ambBases));
            //init on first position
            if( position == 1 ){
                for( Character ac : ambBases ){
                    allProbeSeqs.add(new StringBuilder(ac.toString()));
                }
                //System.out.println("" + allProbeSeqs.toString());
            }
            else{
                //simple addition of char
                if( ambBases.length == 1 ){
                    for( StringBuilder sb : allProbeSeqs ){
                        sb.append(ambBases[0]);
                    }
                }
                //expand
                else{
                    for( int i=0; i<ambBases.length; i++){
                        if( i==0 ){
                            for( StringBuilder sb : allProbeSeqs ){
                                tempSeqs.add(new StringBuilder(sb).append(ambBases[0].toString()));
                            }
                        }
                        else{
                            for( StringBuilder sb : allProbeSeqs ){
                                tempSeqs.add(new StringBuilder(sb).append(ambBases[i].toString()));
                            }
                        }
                    }
                    allProbeSeqs.clear();
                    allProbeSeqs.addAll(tempSeqs);
                    tempSeqs.clear();
                }
            }
        }
        List<Probe> probes = new ArrayList<Probe>();
        int count = 0;
        for(StringBuilder sb : allProbeSeqs){
            count++;
            probes.add(new Probe(getName() + count, sb.toString(), isForwardStrand(), getTargetNodeIds()));
        }
        Collections.sort(probes, new Comparator<Probe>(){

            @Override
            public int compare(Probe o1, Probe o2) {
                return o1.getSequence().compareTo(o2.getSequence());
            }
        });
        return probes;
    }

    public void reverseComplement() {
        //System.out.println("before RC " + this.sequence);
        this.sequence = NucleicAcidSequence.reverseComplement(sequence);
        //System.out.println("after RC  " + this.sequence);
    }
    
    /**
     * clones this probe into an exact duplicate with all fields copied from this object
     * @return probeClone
     */
    public Probe clone(){
        List<Integer> targetNodeIdsClone = new ArrayList<Integer>();
        targetNodeIdsClone.addAll(this.targetNodeIds);
        
        Probe clone = new Probe(this.name, this.sequence, this.forwardStrand, targetNodeIdsClone);
        return clone;
    }
}
