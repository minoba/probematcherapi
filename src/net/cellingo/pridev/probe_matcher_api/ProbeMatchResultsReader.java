/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.io.IOException;

/**
 *
 * @author Gebruiker
 */
public interface ProbeMatchResultsReader {
    /**
     * single method for reading ProbeMatchResults object from file
     * @param fileName
     * @return probeMatchResults
     * @throws IOException 
     */
    public ProbeMatchResults read(String fileName) throws IOException;
}
