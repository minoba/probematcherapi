
package net.cellingo.pridev.probe_matcher_api;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Encapsulates all analysis results of a matching of a probe or primer pair against a sequence collection.
 * @author m.a.noback (michiel@cellingo.net)
 */
public class ProbeMatchResults {
    private final HashMap<Integer, ProbeMatchStatistics> targetGroupStats = new HashMap<Integer, ProbeMatchStatistics>();
    /**
     * this field encapsulates all represented sequences and nodes in the scanned sequence (search space)
     */
    private final HashMap<Integer, Integer> taxNodeRepresentation = new HashMap<Integer, Integer>();
    private int treeSize;
    private HashSet<Integer> unfetchableNodes;
    private int sequenceCount;
    private Probe forwardProbe;
    private Probe reverseProbe;
    private List<Probe> expandedProbesFW;
    private List<Probe> expandedProbesRV;
    private HashMap<String, String> analysisSettings = new HashMap<String, String>();
    private int unfetchableNodeCount;

        
    public HashSet<Integer> getUnfetchableNodes() {
        return unfetchableNodes;
    }

    public void setUnfetchableNodes(HashSet<Integer> unfetchableNodes) {
        this.unfetchableNodes = unfetchableNodes;
    }

    public int getSequenceCount() {
        return sequenceCount;
    }

    public void setSequenceCount(int sequenceCount) {
        this.sequenceCount = sequenceCount;
    }

    public Probe getForwardProbe() {
        return forwardProbe;
    }

    public void setForwardProbe(Probe forwardProbe) {
        this.forwardProbe = forwardProbe;
    }

    public Probe getReverseProbe() {
        return reverseProbe;
    }

    public void setReverseProbe(Probe reverseProbe) {
        this.reverseProbe = reverseProbe;
    }

    public List<Probe> getExpandedProbesFW() {
        return expandedProbesFW;
    }

    public void setExpandedProbesFW(List<Probe> expandedProbesFW) {
        this.expandedProbesFW = expandedProbesFW;
    }

    public List<Probe> getExpandedProbesRV() {
        return expandedProbesRV;
    }

    public void setExpandedProbesRV(List<Probe> expandedProbesRV) {
        this.expandedProbesRV = expandedProbesRV;
    }

    public HashMap<Integer, ProbeMatchStatistics> getTargetGroupStats() {
        return targetGroupStats;
    }

    public int getTreeSize() {
        return treeSize;
    }

    public HashMap<String, String> getAnalysisSettings() {
        return analysisSettings;
    }
    
    /**
     * registers the size of the taxonomic tree used in this analysis
     * @param treeSize 
     */
    public void setTreeSize(int treeSize){
        this.treeSize = treeSize;
    }
    
    /**
     * this method can be used to keep track of the taxIDs that were represented in the search space
     * 
     * @param taxID will add 1 to the key of taxID if present else will create the key with value 1
     */
    public void addRepresentedTaxID(int taxID){
        if( taxNodeRepresentation.containsKey( taxID ) ){
            taxNodeRepresentation.put(taxID, taxNodeRepresentation.get(taxID)+1);
        }
        else{
            taxNodeRepresentation.put(taxID, 1);
        }
    }

    public void setRepresentedTaxID(int taxID, int count){
        taxNodeRepresentation.put(taxID, count);
    }
    
    /**
     * returns a map with taxID keys and occurrence counts as values
     * @return 
     */
    public HashMap<Integer, Integer> getRepresentedTaxIDs(){
        return taxNodeRepresentation;
    }

    /**
     * adds a matchstats object for 
     * @param matchStats 
     */
    public void addTargetGroupProbeMatchStatistics(int targetTaxID, ProbeMatchStatistics matchStats) {
        this.targetGroupStats.put(targetTaxID, matchStats);
    }
    
    /**
     * returns the ProbeMatchStatistics object associated with the given taxID
     * @param taxID
     * @return ProbeMatchStatistics
     */
    public ProbeMatchStatistics getProbeMatchStatistics(int taxID){
        return this.targetGroupStats.get(taxID);
    }

    void setUnfetchableNodesCount(int unfetchableNodeCount) {
        this.unfetchableNodeCount = unfetchableNodeCount;
    }
}
