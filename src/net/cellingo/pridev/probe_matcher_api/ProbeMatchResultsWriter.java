/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.io.IOException;
import java.util.Properties;

/**
 * Interface specifying a single write method for writing probe analysis results
 * @author m.a.noback (michiel@cellingo.net)
 */
public interface ProbeMatchResultsWriter {
    /**
     * write method to write probeMatchResults to a specified file
     * @param probeMatchResults
     * @param properties the analysis settings as properties object (can be null, then the settings should be read from the probeMatchResults object, if available)
     * @throws IOException 
     */
    public void write(ProbeMatchResults probeMatchResults, Properties properties) throws IOException;
}
