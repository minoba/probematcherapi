/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

/**
 *
 * @author Gebruiker
 */
public enum TaxNodeMatchResultsFilter {
    IN_TARGET_GROUP_WITH_FORWARD_STRAND_MATCH{
        @Override
        public boolean filter(TaxNodeMatchResults taxNodeMatchResults){
            return( taxNodeMatchResults.isTargetGroup() && taxNodeMatchResults.getForwardStrandMatchCount() > 0 );
        }
    },
    IN_TARGET_GROUP_WITH_REVERSE_STRAND_MATCH{
        @Override
        public boolean filter(TaxNodeMatchResults taxNodeMatchResults){
            return( taxNodeMatchResults.isTargetGroup() && taxNodeMatchResults.getReverseStrandMatchCount() > 0 );
        }
    },
    IN_TARGET_GROUP_WITH_FORWARD_AND_REVERSE_STRAND_MATCH{
        @Override
        public boolean filter(TaxNodeMatchResults taxNodeMatchResults){
            return( taxNodeMatchResults.isTargetGroup() && taxNodeMatchResults.getBothStrandMatchCount() > 0 );
        }
    },

    OUTSIDE_TARGET_GROUP_WITH_FORWARD_STRAND_MATCH{
        @Override
        public boolean filter(TaxNodeMatchResults taxNodeMatchResults){
            return( ! taxNodeMatchResults.isTargetGroup() && taxNodeMatchResults.getForwardStrandMatchCount() > 0 );
        }
    },
    OUTSIDE_TARGET_GROUP_WITH_REVERSE_STRAND_MATCH{
        @Override
        public boolean filter(TaxNodeMatchResults taxNodeMatchResults){
            return( ! taxNodeMatchResults.isTargetGroup() && taxNodeMatchResults.getReverseStrandMatchCount() > 0 );
        }
    },
    OUTSIDE_TARGET_GROUP_WITH_FORWARD_AND_REVERSE_STRAND_MATCH{
        @Override
        public boolean filter(TaxNodeMatchResults taxNodeMatchResults){
            return( ! taxNodeMatchResults.isTargetGroup() && taxNodeMatchResults.getBothStrandMatchCount() > 0 );
        }
    };
    
    /**
     * performs a filtering on the given taxNodeMatchResults and returns true if it passes the filter
     * @param taxNodeMatchResults
     * @return 
     */
    public boolean filter(TaxNodeMatchResults taxNodeMatchResults){
        return true;
    }
}
