/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.io.IOException;
import nl.bioinf.noback.taxonomy.model.TaxTree;

/**
 * Probe matcher interface specifying methods to perform probe matching
 * @author m.a.noback (michiel@cellingo.net)
 */
public interface ProbeMatcher {
    /**
     * Matches a single probe/primer against all sequences of a sequence collection
     * @param taxTree
     * @param sequenceFileName
     * @param probe
     * @param maxAllowedMismatches
     * @param maxMismatches3PrimeEnd maximum mismatches in 3'end which is 5 bases in size
     * @param alsoReverseComplement flag to also search on the reverse complement strand of each sequence
     * @return ProbeMatchResults an object encapsulatinng all analysis results (amongst which Map of ProbeMatchStatistics, where the keys are the target group TaxIDs)
     * @throws IOException 
     */
    public ProbeMatchResults matchSingleProbe(
            TaxTree taxTree, 
            String sequenceFileName, 
            Probe probe, 
            int maxAllowedMismatches, 
            int maxMismatches3PrimeEnd,
            boolean alsoReverseComplement) throws IOException;
    
    /**
     * Matches a primer pair against all sequences of a sequence collection
     * @param taxTree
     * @param sequenceFileName
     * @param probeFW
     * @param probeRV
     * @param maxAllowedMismatches
     * @param maxMismatches3PrimeEnd maximum mismatches in 3'end which is 5 bases in size
     * @param maxDistance tha maximum distance between two primers for a legal match
     * @param alsoReverseComplement flag to also search on the reverse complement strand of each sequence
     * @return ProbeMatchResults an object encapsulatinng all analysis results (amongst which Map of ProbeMatchStatistics, where the keys are the target group TaxIDs)
     * @throws IOException 
     */
    public ProbeMatchResults matchPrimerPair(
            TaxTree taxTree, 
            String sequenceFileName, 
            Probe probeFW, 
            Probe probeRV, 
            int maxAllowedMismatches, 
            int maxMismatches3PrimeEnd, 
            int maxDistance,
            boolean alsoReverseComplement) throws IOException;
    
}
