/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.util.HashSet;

/**
 *
 * @author Gebruiker
 */
public class MatchResult {
        
    private String matchString;
    private int matchIndex;
    private int mismatchCount; 
//    private boolean withinTargetGroup;
    private HashSet<Integer> matchedTaxIDs;

    public MatchResult(){}
    
    public MatchResult(String matchString, int matchIndex, int mismatchCount) {
        this.matchString = matchString;
        this.matchIndex = matchIndex;
        this.mismatchCount = mismatchCount;
    }

    /**
     * add the taxID of a target group that was matched
     * @param targetGroupTaxID 
     */
    public void addMatchedTaxID(int targetGroupTaxID ){
        if(this.matchedTaxIDs == null){
            matchedTaxIDs = new HashSet<Integer>();
        }
        matchedTaxIDs.add(targetGroupTaxID);
    }    
    
    /**
     * returns the taxIDs of the target groups that were matched
     * @return 
     */
    public HashSet<Integer> getMatchedTaxIDs(){
        return this.matchedTaxIDs;
    }
//    public boolean isWithinTargetGroup() {
//        return withinTargetGroup;
//    }
//
//    public void setWithinTargetGroup(boolean withinTargetGroup) {
//        this.withinTargetGroup = withinTargetGroup;
//    }

    
    public String getMatchString() {
        return matchString;
    }

    public int getMatchIndex() {
        return matchIndex;
    }

    public int getMismatchCount() {
        return mismatchCount;
    }

    public void setMatchString(String matchString) {
        this.matchString = matchString;
    }

    public void setMatchIndex(int matchIndex) {
        this.matchIndex = matchIndex;
    }

    public void setMismatchCount(int count) {
        this.mismatchCount = count;
    }

    @Override
    public String toString() {
        return "MatchResult{" + "matchString=" + matchString + ", matchIndex=" + matchIndex + ", mismatchCount=" + mismatchCount + '}';
    }

}
