/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

/**
 * simple data class for encapsulating probe match data 
 * @author michiel
 */
public class TaxNodeMatchResults {
    private int taxID;
    private boolean targetGroup;
    private int forwardMatchCount;
    private int reverseMatchCount;
    private int bothStrandMatchCount;

    public TaxNodeMatchResults(int taxID, boolean targetGroup, int forwardMatchCount, int reverseMatchCount, int bothStrandMatchCount) {
        this.taxID = taxID;
        this.targetGroup = targetGroup;
        this.forwardMatchCount = forwardMatchCount;
        this.reverseMatchCount = reverseMatchCount;
        this.bothStrandMatchCount = bothStrandMatchCount;
    }

    public TaxNodeMatchResults(int taxID, boolean targetGroup, int forwardMatchCount) {
        this.taxID = taxID;
        this.targetGroup = targetGroup;
        this.forwardMatchCount = forwardMatchCount;
    }

    
    public TaxNodeMatchResults(int taxID, boolean targetGroup) {
        this.taxID = taxID;
        this.targetGroup = targetGroup;
    }

    public int getTaxID() {
        return taxID;
    }
    
    /**
     * specifies whether this TaxNodeMatchResult is member of the target group for the primer (pair)
     * @param targetGroup
     */
    public void setTargetGroup(boolean targetGroup) {
        this.targetGroup = targetGroup;
    }
    
    /**
     * specifies whether this TaxNodeMatchResult is member of the target group for the primer (pair)
     * @return isTargetGroup
     */
    public boolean isTargetGroup() {
        return targetGroup;
    }

    public int getForwardStrandMatchCount() {
        return forwardMatchCount;
    }

    public int getReverseStrandMatchCount() {
        return reverseMatchCount;
    }

    public int getBothStrandMatchCount() {
        return bothStrandMatchCount;
    }
    
    /**
     * adds a single match for a forward-strand targeted probe
     */
    public void addForwardMatch(){
        this.forwardMatchCount++;
    }
    
    public void addReverseMatch(){
        this.reverseMatchCount++;
    }
 
    public void addBothStrandsMatch(){
        this.bothStrandMatchCount++;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.taxID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TaxNodeMatchResults other = (TaxNodeMatchResults) obj;
        if (this.taxID != other.taxID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TaxNodeMatchResults{" + "taxID=" + taxID + ", targetGroup=" + targetGroup + ", forwardMatchCount=" + forwardMatchCount + ", reverseMatchCount=" + reverseMatchCount + ", bothStrandMatchCount=" + bothStrandMatchCount + '}';
    }
    
    
}
