/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * XML writer implementation for ProbeMatchResults
 *
 * @author m.a.noback (michiel@cellingo.net)
 */
public class ProbeMatchResultsWriterXMLimpl implements ProbeMatchResultsWriter {

    private final String fileName;
    private ProbeMatchResults probeMatchResults;
    private Properties properties;
    private AnalysisMode analysisMode;

    public ProbeMatchResultsWriterXMLimpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void write(ProbeMatchResults probeMatchResults, Properties props) throws IOException {
        //System.out.println("[1] "+props);
        //System.out.println("[2] "+fileName);
//        System.out.println("[3] "+props.getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE));
//        System.out.println("[4] "+ AnalysisMode.valueOf(props.getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE)));
        this.probeMatchResults = probeMatchResults;
        this.properties = props;

        if (fileName == null) {
            throw new IllegalArgumentException("No output file was specified");
        }

        this.analysisMode = AnalysisMode.valueOf(props.getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE));

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            
            Element rootElement = doc.createElement(ProbeMatcherStringConstants.ROOT);
            doc.appendChild(rootElement);
            rootElement.setAttribute("xmlns", ProbeMatcherStringConstants.PROBE_MATCH_DATA);
            
            Element pmrElement = doc.createElement(ProbeMatcherStringConstants.PROBE_MATCH_DATA);
            rootElement.appendChild(pmrElement);

            /*write the settings node*/
            Element settings = doc.createElement(ProbeMatcherStringConstants.ANALYSIS_SETTINGS);
            pmrElement.appendChild(settings);
            fillSettingsNode(doc, settings);

            /*write meta results*/
            Element metaStats = doc.createElement(ProbeMatcherStringConstants.META_DATA);
            pmrElement.appendChild(metaStats);
            fillMetaStatsNode(doc, metaStats);
            
            /*write match results*/
            Element matchStats = doc.createElement(ProbeMatcherStringConstants.MATCH_STATISTICS);
            pmrElement.appendChild(matchStats);
            HashMap<Integer, ProbeMatchStatistics> targetGroupStats = probeMatchResults.getTargetGroupStats();
            for( Integer targetGroupId : targetGroupStats.keySet()){
                ProbeMatchStatistics pms = targetGroupStats.get(targetGroupId);
                
                Element matchStat = doc.createElement(ProbeMatcherStringConstants.MATCH_STATISTIC);
                matchStats.appendChild(matchStat);
                
                Attr attr = doc.createAttribute(ProbeMatcherStringConstants.TARGET_GROUP_ID);
                attr.setValue(Integer.toString(targetGroupId));
                matchStat.setAttributeNode(attr);
                
                //match metadata
                fillMatchStatsNode(doc, matchStat, pms);
                
                if(analysisMode == AnalysisMode.PRIMER_PAIR){
                    fillFragmentSizesNode(doc, matchStat, pms);
                }
                
                //per-Taxnode match results
                Element nodeMatches = doc.createElement(ProbeMatcherStringConstants.TAXNODE_MATCHES);
                matchStat.appendChild(nodeMatches);
                fillTaxnodeMatchNode(doc, nodeMatches, pms);

                
            }
            //per_taxnode sequence search space
            Element sequenceRepresentedNodes = doc.createElement(ProbeMatcherStringConstants.SEQUENCE_REPRESENTED_NODES);
            pmrElement.appendChild(sequenceRepresentedNodes);
            fillSequenceRepresentedNodes(doc, sequenceRepresentedNodes);

            //write to actual file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(fileName));
            //System.out.println("[3] $$$$$$$$$$$$$ WRITING TO XML FILE $$$$$$$$$$$$$$$$$$");

            // Output to console for testing
            StreamResult resultCons = new StreamResult(System.out);

            //transformer.transform(source, resultCons);
            //System.out.println("[4] $$$$$$$$$$$$$ WRITING TO XML FILE $$$$$$$$$$$$$$$$$$");
            transformer.transform(source, result);
            //System.out.println("$$$$$$$$$$$$$END  WRITING TO XML FILE $$$$$$$$$$$$$$$$$$");
            
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
//            throw new IOException("Unable to create xml document, cause=" + ex.getCause() + "; message= " + ex.getMessage());
        } catch (TransformerConfigurationException ex) {
            ex.printStackTrace();
//            throw new IOException("Unable to create xml document, cause=" + ex.getCause() + "; message= " + ex.getMessage());
        } catch (TransformerException ex) {
            ex.printStackTrace();
//            throw new IOException("Unable to write to xml document, cause=" + ex.getCause() + "; message= " + ex.getMessage());
        }

    }

    
    private void fillFragmentSizesNode(Document doc, Element matchStat, ProbeMatchStatistics pms) {
        Element targetGroupFragmentSizes = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_FRAGMENT_SIZES);
        matchStat.appendChild(targetGroupFragmentSizes);
        HashMap<Integer, Integer> tgSizes = pms.getTargetGroupsFragmentSizes();
        List<Integer> sizes = new ArrayList<Integer>();
        sizes.addAll( tgSizes.keySet() );
        Collections.sort(sizes);
        for(int size : sizes){
            Element sizeCount = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_FRAGMENT_SIZE);
            targetGroupFragmentSizes.appendChild(sizeCount);
            
            Attr attr = doc.createAttribute(ProbeMatcherStringConstants.SIZE);
            attr.setValue(Integer.toString(size));
            sizeCount.setAttributeNode(attr);

            attr = doc.createAttribute(ProbeMatcherStringConstants.COUNT);
            attr.setValue(Integer.toString(tgSizes.get(size)));
            sizeCount.setAttributeNode(attr);
            
        }

        Element nonTargetGroupFragmentSizes = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_FRAGMENT_SIZES);
        matchStat.appendChild(nonTargetGroupFragmentSizes);
        tgSizes = pms.getNonTargetGroupsFragmentSizes();
        sizes.clear();
        sizes.addAll( tgSizes.keySet() );
        Collections.sort(sizes);
        for(int size : sizes){
            Element sizeCount = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_FRAGMENT_SIZE);
            nonTargetGroupFragmentSizes.appendChild(sizeCount);
            
            Attr attr = doc.createAttribute(ProbeMatcherStringConstants.SIZE);
            attr.setValue(Integer.toString(size));
            sizeCount.setAttributeNode(attr);

            attr = doc.createAttribute(ProbeMatcherStringConstants.COUNT);
            attr.setValue(Integer.toString(tgSizes.get(size)));
            sizeCount.setAttributeNode(attr);
            
        }
        
    }
    
    private void fillSettingsNode(Document doc, Element settings) {
        Element date = doc.createElement(ProbeMatcherStringConstants.TIME_STAMP);
        date.appendChild(doc.createTextNode(new Date().toString()));
        settings.appendChild(date);
        
        //System.out.println("[1] ProbeMatcherStringConstants.ANALYSIS_MODE=" + properties.getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE));
        Element mode = doc.createElement(ProbeMatcherStringConstants.ANALYSIS_MODE);
        mode.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.ANALYSIS_MODE)));
        settings.appendChild(mode);

        //System.out.println("[2] ProbeMatcherStringConstants.SEQUENCE_FILE_PATH=" + properties.getProperty(ProbeMatcherStringConstants.SEQUENCE_FILE_PATH));
        Element seqFile = doc.createElement(ProbeMatcherStringConstants.SEQUENCE_FILE_PATH);
        seqFile.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.SEQUENCE_FILE_PATH)));
        settings.appendChild(seqFile);

        //System.out.println("[3] ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH=" + properties.getProperty(ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH));
        Element ncbiFile = doc.createElement(ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH);
        ncbiFile.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.NCBI_DUMP_FILE_PATH)));
        settings.appendChild(ncbiFile);

        
        //System.out.println("[4] ProbeMatcherStringConstants.FORWARD_PROBE_NAME=" + properties.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_NAME));
        Element fwProbeName = doc.createElement(ProbeMatcherStringConstants.FORWARD_PROBE_NAME);
        fwProbeName.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_NAME)));
        settings.appendChild(fwProbeName);

       // System.out.println("[5] ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE=" + properties.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE));
        Element fwProbeSeq = doc.createElement(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE);
        fwProbeSeq.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE)));
        settings.appendChild(fwProbeSeq);

        if (analysisMode == AnalysisMode.PRIMER_PAIR) {
            //System.out.println("[6] ProbeMatcherStringConstants.REVERSE_PROBE_NAME=" + properties.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE));
            Element rvProbeName = doc.createElement(ProbeMatcherStringConstants.REVERSE_PROBE_NAME);
            rvProbeName.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.REVERSE_PROBE_NAME)));
            settings.appendChild(rvProbeName);

            //System.out.println("[7] ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE=" + properties.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE));
            Element rvProbeSeq = doc.createElement(ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE);
            rvProbeSeq.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.REVERSE_PROBE_SEQUENCE)));
            settings.appendChild(rvProbeSeq);

            //System.out.println("[8] ProbeMatcherStringConstants.MAX_DISTANCE=" + properties.getProperty(ProbeMatcherStringConstants.FORWARD_PROBE_SEQUENCE));
            Element maxDistance = doc.createElement(ProbeMatcherStringConstants.MAX_DISTANCE);
            maxDistance.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.MAX_DISTANCE)));
            settings.appendChild(maxDistance);
        }
        //System.out.println("[2] ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT=" + properties.getProperty(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT));
        Element alsoRevCompl = doc.createElement(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT);
        alsoRevCompl.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.ALSO_REVERSE_COMPLEMENT)));
        settings.appendChild(alsoRevCompl);

        //System.out.println("[2] ProbeMatcherStringConstants.TARGET_TAX_IDS=" + properties.getProperty(ProbeMatcherStringConstants.TARGET_TAX_IDS));
        Element targetTaxIds = doc.createElement(ProbeMatcherStringConstants.TARGET_TAX_IDS);
        targetTaxIds.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.TARGET_TAX_IDS)));
        settings.appendChild(targetTaxIds);

        //System.out.println("[2] ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES=" + properties.getProperty(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES));
        Element maxMismatches = doc.createElement(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES);
        maxMismatches.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES)));
        settings.appendChild(maxMismatches);

        //System.out.println("[2] ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME=" + properties.getProperty(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME));
        Element maxMismatches3Prime = doc.createElement(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME);
        maxMismatches3Prime.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.MAX_ALLOWED_MISMATCHES_3PRIME)));
        settings.appendChild(maxMismatches3Prime);

        //System.out.println("[2] ProbeMatcherStringConstants.THREE_PRIME_END_SIZE=" + properties.getProperty(ProbeMatcherStringConstants.THREE_PRIME_END_SIZE));
        Element threePrimeEndSize = doc.createElement(ProbeMatcherStringConstants.THREE_PRIME_END_SIZE);
        threePrimeEndSize.appendChild(doc.createTextNode(properties.getProperty(ProbeMatcherStringConstants.THREE_PRIME_END_SIZE)));
        settings.appendChild(threePrimeEndSize);
    }

    private void fillMetaStatsNode(Document doc, Element metaStats) {
        Element el;
        el = doc.createElement(ProbeMatcherStringConstants.DISAMBIGUED_FORWARD_PROBES);
        for( Probe p : probeMatchResults.getExpandedProbesFW() ){
            Element sel = doc.createElement(ProbeMatcherStringConstants.DISAMBIGUED_PROBE);
            sel.appendChild(doc.createTextNode(p.getSequence()));
            el.appendChild(sel);
        }
        metaStats.appendChild(el);

        if(analysisMode == AnalysisMode.PRIMER_PAIR ){
            el = doc.createElement(ProbeMatcherStringConstants.DISAMBIGUED_REVERSE_PROBES);
            for( Probe p : probeMatchResults.getExpandedProbesRV() ){
                Element sel = doc.createElement(ProbeMatcherStringConstants.DISAMBIGUED_PROBE);
                sel.appendChild(doc.createTextNode(p.getSequence()));
                el.appendChild(sel);
            }
            metaStats.appendChild(el);
        }
        
        el = doc.createElement(ProbeMatcherStringConstants.TREE_SIZE);
        el.appendChild(doc.createTextNode( Integer.toString(probeMatchResults.getTreeSize())));
        metaStats.appendChild(el);
        
        el = doc.createElement(ProbeMatcherStringConstants.UNFETCHABLE_NODES);
        el.appendChild(doc.createTextNode( Integer.toString(probeMatchResults.getUnfetchableNodes().size())));
        metaStats.appendChild(el);
        
        el = doc.createElement(ProbeMatcherStringConstants.SEQUENCE_COUNT);
        el.appendChild(doc.createTextNode( Integer.toString(probeMatchResults.getSequenceCount())));
        metaStats.appendChild(el);
    }

    private void fillMatchStatsNode(Document doc, Element matchStats, ProbeMatchStatistics pms) {
        Element el;
        el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_SPECIES_COUNT);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getTargetGroupSpeciesNumber()) ));
        matchStats.appendChild(el);

        el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_LEAF_COUNT);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getTargetGroupLeafNumber()) ));
        matchStats.appendChild(el);
        
        el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_REPRESENTED_SEQUENCES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getTargetGroupSequences()) ));
        matchStats.appendChild(el);

        el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_REPRESENTED_NODES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getTargetGroupRepresentedNodes()) ));
        matchStats.appendChild(el);

        el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_REPRESENTED_SEQUENCES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getNonTargetGroupSequences()) ));
        matchStats.appendChild(el);
        
        el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_REPRESENTED_NODES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getNonTargetGroupRepresentedNodes()) ));
        matchStats.appendChild(el);
        
        //FW probe match results
        el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_FW_PROBE_MATCHED_SEQUENCES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getProbeFWmatchedTargetGroupSequences()) ));
        matchStats.appendChild(el);

        el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_FW_PROBE_MATCHED_NODES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getPassedCount(TaxNodeMatchResultsFilter.IN_TARGET_GROUP_WITH_FORWARD_STRAND_MATCH)) ));
        matchStats.appendChild(el);
        
        el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_FW_PROBE_MATCHED_SEQUENCES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getProbeFWmatchedNonTargetGroupSequences()) ));
        matchStats.appendChild(el);

        el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_FW_PROBE_MATCHED_NODES);
        el.appendChild(doc.createTextNode( Integer.toString(pms.getPassedCount(TaxNodeMatchResultsFilter.OUTSIDE_TARGET_GROUP_WITH_FORWARD_STRAND_MATCH)) ));
        matchStats.appendChild(el);

        if(analysisMode == AnalysisMode.PRIMER_PAIR){
            //RV probe match results
            el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_RV_PROBE_MATCHED_SEQUENCES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getProbeRVmatchedTargetGroupSequences()) ));
            matchStats.appendChild(el);

            el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_RV_PROBE_MATCHED_NODES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getPassedCount(TaxNodeMatchResultsFilter.IN_TARGET_GROUP_WITH_REVERSE_STRAND_MATCH)) ));
            matchStats.appendChild(el);

            el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_RV_PROBE_MATCHED_SEQUENCES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getProbeRVmatchedNonTargetGroupSequences()) ));
            matchStats.appendChild(el);

            el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_RV_PROBE_MATCHED_NODES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getPassedCount(TaxNodeMatchResultsFilter.OUTSIDE_TARGET_GROUP_WITH_REVERSE_STRAND_MATCH)) ));
            matchStats.appendChild(el);
            
            //probe combi match resuls
            el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_BOTH_PROBES_MATCHED_SEQUENCES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getProbeCombiMatchedTargetGroupSequences()) ));
            matchStats.appendChild(el);

            el = doc.createElement(ProbeMatcherStringConstants.TARGET_GROUP_BOTH_PROBES_MATCHED_NODES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getPassedCount(TaxNodeMatchResultsFilter.IN_TARGET_GROUP_WITH_FORWARD_AND_REVERSE_STRAND_MATCH)) ));
            matchStats.appendChild(el);

            el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_BOTH_PROBES_MATCHED_SEQUENCES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getProbeCombiMatchedNonTargetGroupSequences()) ));
            matchStats.appendChild(el);
            
            el = doc.createElement(ProbeMatcherStringConstants.NON_TARGET_GROUP_BOTH_PROBES_MATCHED_NODES);
            el.appendChild(doc.createTextNode( Integer.toString(pms.getPassedCount(TaxNodeMatchResultsFilter.OUTSIDE_TARGET_GROUP_WITH_FORWARD_AND_REVERSE_STRAND_MATCH)) ));
            matchStats.appendChild(el);
        }
        
    }

    private void fillTaxnodeMatchNode(Document doc, Element nodeMatches, ProbeMatchStatistics pms) {
        Element el;
        Iterator<TaxNodeMatchResults> it = pms.iterator(ProbeMatchStatistics.IteratorType.RANDOM);
        while (it.hasNext()) {
            TaxNodeMatchResults tnmr = it.next();
            el = doc.createElement(ProbeMatcherStringConstants.TAXNODE_MATCH);
            nodeMatches.appendChild(el);
            Attr attr = doc.createAttribute(ProbeMatcherStringConstants.PROBE_FW_MATCHES);
            attr.setValue(Integer.toString(tnmr.getForwardStrandMatchCount()));
            el.setAttributeNode(attr);
            if(analysisMode == AnalysisMode.PRIMER_PAIR){
                attr = doc.createAttribute(ProbeMatcherStringConstants.PROBE_RV_MATCHES);
                attr.setValue(Integer.toString(tnmr.getReverseStrandMatchCount()));
                el.setAttributeNode(attr);
                attr = doc.createAttribute(ProbeMatcherStringConstants.PROBE_PAIR_MATCHES);
                attr.setValue(Integer.toString(tnmr.getBothStrandMatchCount()));
                el.setAttributeNode(attr);
            }
            attr = doc.createAttribute(ProbeMatcherStringConstants.TARGET_GROUP);
            attr.setValue(Boolean.toString(tnmr.isTargetGroup()));
            el.setAttributeNode(attr);

            attr = doc.createAttribute(ProbeMatcherStringConstants.TAX_ID);
            attr.setValue(Integer.toString(tnmr.getTaxID()));
            el.setAttributeNode(attr);
        }
    }

    private void fillSequenceRepresentedNodes(Document doc, Element sequenceRepresentedNodes) {
        Element el;
        HashMap<Integer, Integer> representedTaxIDs = this.probeMatchResults.getRepresentedTaxIDs();
        for( Entry<Integer, Integer> entry : representedTaxIDs.entrySet() ){
            el = doc.createElement(ProbeMatcherStringConstants.SEQUENCE_REPRESENTED_NODE);
            sequenceRepresentedNodes.appendChild(el);
            Attr attr = doc.createAttribute(ProbeMatcherStringConstants.COUNT);
            attr.setValue(entry.getValue().toString());
            el.setAttributeNode(attr);
            attr = doc.createAttribute(ProbeMatcherStringConstants.TAX_ID);
            attr.setValue(entry.getKey().toString());
            el.setAttributeNode(attr);
            
        }
    }

}
