/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cellingo.pridev.probe_matcher_api;

/**
 *
 * @author Gebruiker
 */
public class ProbeMatcherStringConstants {
    public static final String ROOT = "root";
    public static final String VERBOSE = "verbose";
    public static final String SEQUENCE_FILE_PATH = "sequence_file";
    public static final String NCBI_DUMP_FILE_PATH = "ncbi_taxdump_file";
    public static final String OUTPUT_FILE_PATH = "output_file";
    public static final String ANALYSIS_MODE = "analysis_mode";
    public static final String MAX_ALLOWED_MISMATCHES = "maximum_allowed_mismatches";
    public static final String MAX_ALLOWED_MISMATCHES_3PRIME = "maximum_allowed_mismatches_3prime";
    public static final String THREE_PRIME_END_SIZE = "three_prime_end_size";
    public static final String FORWARD_PROBE_NAME = "forward_probe_name";
    public static final String FORWARD_PROBE_SEQUENCE = "forward_probe_sequence";
    public static final String REVERSE_PROBE_NAME = "reverse_probe_name";
    public static final String REVERSE_PROBE_SEQUENCE = "reverse_probe_sequence";
    public static final String TARGET_TAX_IDS = "target_tax_ids";
    public static final String MAX_DISTANCE = "maximum_distance";
    public static final String ALSO_REVERSE_COMPLEMENT = "also_reverse_complement";
    public static final String TOP_RANKED_NODES_LIST_LENGTH = "top_ranked_nodes_list_length";

    public static final String PROBE_MATCH_DATA = "probe_match_data";
    public static final String ANALYSIS_SETTINGS = "analysis_settings";
    public static final String META_DATA = "meta_data";
    public static final String MATCH_STATISTICS = "match_statistics";
    public static final String MATCH_STATISTIC = "match_statistic";
    public static final String TARGET_GROUP_ID = "target_group_id";
    public static final String TARGET_GROUP_NAME = "target_group_name";
    public static final String TIME_STAMP = "time_stamp";
    public static final String DISAMBIGUED_FORWARD_PROBES = "disambigued_forward_probes";
    public static final String DISAMBIGUED_REVERSE_PROBES = "disambigued_reverse_probes";
    public static final String DISAMBIGUED_PROBE = "disambigued_probe";
    public static final String TREE_SIZE = "tree_size";
    public static final String UNFETCHABLE_NODES = "unfetchable_nodes";
    public static final String SEQUENCE_COUNT = "sequence_count";
    
    public static final String TARGET_GROUP_SPECIES_COUNT = "target_group_species_count";
    public static final String TARGET_GROUP_LEAF_COUNT = "target_group_leaf_count";
    public static final String TARGET_GROUP_REPRESENTED_SEQUENCES = "target_group_represented_sequences";
    public static final String TARGET_GROUP_REPRESENTED_NODES = "target_group_represented_nodes";
    public static final String TARGET_GROUP_FRAGMENT_SIZES = "target_group_fragment_sizes";
    public static final String TARGET_GROUP_FRAGMENT_SIZE = "target_group_fragment_size";
    public static final String NON_TARGET_GROUP_REPRESENTED_SEQUENCES = "non_target_group_represented_sequences";
    public static final String NON_TARGET_GROUP_REPRESENTED_NODES = "non_target_group_represented_nodes";
    public static final String NON_TARGET_GROUP_FRAGMENT_SIZES = "non_target_group_fragment_sizes";
    public static final String NON_TARGET_GROUP_FRAGMENT_SIZE = "non_target_group_fragment_size";
    
    public static final String TARGET_GROUP_FW_PROBE_MATCHED_SEQUENCES = "target_group_fw_probe_matched_sequences";
    public static final String TARGET_GROUP_FW_PROBE_MATCHED_NODES = "target_group_fw_probe_matched_nodes";
    public static final String TARGET_GROUP_RV_PROBE_MATCHED_SEQUENCES = "target_group_rv_probe_matched_sequences";
    public static final String TARGET_GROUP_RV_PROBE_MATCHED_NODES = "target_group_rv_probe_matched_nodes";
    public static final String TARGET_GROUP_BOTH_PROBES_MATCHED_SEQUENCES = "target_group_both_probes_matched_sequences";
    public static final String TARGET_GROUP_BOTH_PROBES_MATCHED_NODES = "target_group_both_probes_matched_nodes";
    
    public static final String NON_TARGET_GROUP_FW_PROBE_MATCHED_SEQUENCES = "non_target_group_fw_probe_matched_sequences";
    public static final String NON_TARGET_GROUP_FW_PROBE_MATCHED_NODES = "non_target_group_fw_probe_matched_nodes";
    public static final String NON_TARGET_GROUP_RV_PROBE_MATCHED_SEQUENCES = "non_target_group_rv_probe_matched_sequences";
    public static final String NON_TARGET_GROUP_RV_PROBE_MATCHED_NODES = "non_target_group_rv_probe_matched_nodes";
    public static final String NON_TARGET_GROUP_BOTH_PROBES_MATCHED_SEQUENCES = "non_target_group_both_probes_matched_sequences";
    public static final String NON_TARGET_GROUP_BOTH_PROBES_MATCHED_NODES = "non_target_group_both_probes_matched_nodes";
    
    
    public static final String TAXNODE_MATCHES = "taxnode_matches";
    public static final String TAXNODE_MATCH = "taxnode_match";
    public static final String TAX_ID = "tax_id";
    public static final String TARGET_GROUP_MEMBER = "is_target_group";
    public static final String PROBE_FW_MATCHES = "fw_matches";
    public static final String PROBE_RV_MATCHES = "rv_matches";
    public static final String PROBE_PAIR_MATCHES = "combi_matches";
    public static final String TARGET_GROUP = "target_group";
    
    public static final String SEQUENCE_REPRESENTED_NODES = "sequence_represented_nodes";
    public static final String SEQUENCE_REPRESENTED_NODE = "node";
    public static final String COUNT = "count";
    public static final String SIZE = "size";
}
